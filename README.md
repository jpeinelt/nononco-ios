# Nononco

Nononco is a small chat platform which is inspired by the famous Yo chat app.
While Yo in its orginal form just let users send a short Yo! to each other Nononco 
goes a step further. Users can ask others specific questions and the receiver of a question
can answer only exactly this answer. This leads to a NO NONsense COmmunication,
hencs the name Nononco.
For more information see [Nononco Server](https://gitlab.com/jpeinelt/nononco-server)

## client code
This repository contains a small and simple demo for ios. It uses Alamofire for
JSON and REST handling.

The code is probably not the nicest, tests are non existing but in the end, it's
just a demo I made to present it at [Berlin Hack and Tell](http://www.meetup.com/de-DE/Berlin-Hack-and-Tell/)
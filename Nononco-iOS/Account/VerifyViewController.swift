//
//  VerifyViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 03/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit

class VerifyViewController: UIViewController {

    var userEmail: String?

    
    @IBOutlet weak var verifyPIN: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.verifyPIN.delegate = self
        self.verifyPIN.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func verify(sender: UIButton) {
        guard userEmail != nil else {
            return
        }
        if let pin = verifyPIN.text {
            UserManager.sharedInstance.registerWithPin(pin, email: userEmail!, completionHandler: {
                (success, error) in
                if success {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewControllerWithIdentifier("RegisteredPath")
                    UIApplication.sharedApplication().delegate?.window?!.rootViewController = viewController
                } else {
                    debugPrint("verifying PIN error: \(error)")
                    let alert = UIAlertController(title: nil, message: "Email is not valid", preferredStyle: UIAlertControllerStyle.Alert)
                    let action = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                        alert.dismissViewControllerAnimated(true, completion: nil)
                        self.navigationController?.popViewControllerAnimated(true)
                    })
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }

    }
}

extension VerifyViewController: UITextFieldDelegate {
}

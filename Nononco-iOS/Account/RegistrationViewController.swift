//
//  RegistrationViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 20/06/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class RegistrationViewController: UIViewController {

    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var registerButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        userEmail.delegate = self
        userEmail.becomeFirstResponder()
    }

    @IBAction func register() {
        if let email = userEmail.text {
            if (email.characters.count == 0  || !isValidEmailAddress(email)) {
                let alert = UIAlertController(title: nil, message: "Email is not valid", preferredStyle: UIAlertControllerStyle.Alert)
                let action = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                })
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                let userManager = UserManager.sharedInstance
                userManager.requestRegistrationWithUsername(email, completionHandler: {
                    success, error in
                    if success {
                        self.performSegueWithIdentifier("ShowVerifySegue", sender: self)
                    } else {
                        let alert = UIAlertController(title: "Server Error", message: error.debugDescription, preferredStyle: UIAlertControllerStyle.Alert)
                        let action = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                            alert.dismissViewControllerAnimated(true, completion: nil)
                        })
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                })
            }
        }
    }

    func isValidEmailAddress(address: String) -> Bool {
        //TODO: maybe use https://github.com/adamwaite/AJWValidator 
        //          or https://documentation.mailgun.com/api-email-validation.html#email-validation instead of regex
        let emailRegex = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}" +
        "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
        "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
        "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
        "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
        "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", argumentArray: [emailRegex])
        return emailTest.evaluateWithObject(address)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowVerifySegue" {
            let verifyViewController = segue.destinationViewController as! VerifyViewController
            verifyViewController.userEmail = userEmail.text
        }
    }
}

extension RegistrationViewController: UITextFieldDelegate {
}

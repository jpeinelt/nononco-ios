//
//  LocationAnswerViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 13/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationAnswerViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!

    var dialog: Dialog?
    private var location: [Double]?
    private var locationManager: CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        self.map.delegate = self
        self.map.setUserTrackingMode(.Follow, animated: true)
        self.map.showsUserLocation = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }

    @IBAction func confirm(button: UIButton) {
        guard dialog != nil && location != nil else {
            return
        }
        self.dialog?.answer = self.location!
        DialogManager.sharedInstance.answerDialog(self.dialog!)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    @IBAction func reject(button: UIButton) {
        guard dialog != nil else {
            return
        }
        DialogManager.sharedInstance.ingnoreDialog(self.dialog!)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

}


extension LocationAnswerViewController: MKMapViewDelegate {
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        let location = userLocation.coordinate
        let region = MKCoordinateRegionMakeWithDistance(location, 1000, 1000)
        self.map.setRegion(region, animated: true)
        self.location = [location.latitude, location.longitude]
    }
}

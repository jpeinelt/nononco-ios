//
//  DateAnswerViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 13/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit

class DateAnswerViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!

    var dialog: Dialog?
    private var date: NSDate = NSDate()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.datePicker.addTarget(self, action: Selector("dateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func confirm(button: UIButton) {
        guard dialog != nil else {
            return
        }
        self.dialog?.answer = self.date
        DialogManager.sharedInstance.answerDialog(self.dialog!)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    @IBAction func reject(button: UIButton) {
        guard dialog != nil else {
            return
        }
        DialogManager.sharedInstance.ingnoreDialog(self.dialog!)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    @objc func dateChanged(picker: UIDatePicker) {
        self.date = picker.date
    }
}

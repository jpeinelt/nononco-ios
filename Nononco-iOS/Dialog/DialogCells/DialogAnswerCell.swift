//
//  DialogAnswerCell.swift
//  Nononco-iOS
//
//  Created by Julius on 11/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit

protocol AnswerCellDelegate {
    func answerWasPressed(dialog: Dialog)
}

class DialogAnswerCell: UITableViewCell {


    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerButton: UIButton!

    var delegate: AnswerCellDelegate?
    private var dialog: Dialog?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dialog = nil
        delegate = nil
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(dialog: Dialog) {
        self.dialog = dialog
        let addressManager = AddressBookManager.sharedInstance
        let name = addressManager.getNameFor(dialog.sender)
        self.questionLabel.text = "\(name) asks: \(Dialog.getQuestionFor(dialog.question))"
    }


    @IBAction func answerPressed(sender: UIButton) {
        if let d = self.delegate {
            d.answerWasPressed(self.dialog!)
        }
    }
    
}

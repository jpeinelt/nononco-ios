//
//  DialogLocationCell.swift
//  Nononco-iOS
//
//  Created by Julius on 11/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit
import MapKit

class DialogLocationCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerMap: MKMapView!

    private var dialog: Dialog?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.answerMap.delegate = self
        dialog = nil
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(dialog: Dialog) {
        self.dialog = dialog
        let addressManager = AddressBookManager.sharedInstance
        var name = addressManager.getNameFor(dialog.receiver)
        self.questionLabel.text = "\(name) sent you a location."

        // if user is receiver and answered
        let username = UserManager.sharedInstance.getCredentials().username
        if let receiver = dialog.receiver {
            if receiver == username {
                name = addressManager.getNameFor(dialog.sender)
                self.questionLabel.text = "You sent \(name) this location:"
            }
        }

        let location = MKPointAnnotation()
        if let coordinates = self.dialog!.answer as? [Double] {
            location.coordinate = CLLocationCoordinate2DMake(coordinates[0], coordinates[1])
            self.answerMap.addAnnotation(location)
            self.answerMap.showAnnotations([location], animated: true)
        }
    }
    
}

extension DialogLocationCell: MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        guard dialog != nil else {
            return nil
        }
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        return annotationView
    }
}

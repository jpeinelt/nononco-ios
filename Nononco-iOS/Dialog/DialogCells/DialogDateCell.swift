//
//  DialogDateCell.swift
//  Nononco-iOS
//
//  Created by Julius on 11/10/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import UIKit

class DialogDateCell: UITableViewCell {


    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(dialog: Dialog) {
        let addressManager = AddressBookManager.sharedInstance
        var name = addressManager.getNameFor(dialog.receiver)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm  dd. LLL YYYY"
        self.questionLabel.text = "\(name) send you this date:"
        self.answerLabel.text = "\(dateFormatter.stringFromDate(NSDate(timeIntervalSince1970:Double(dialog.answer as! Double))))"

        // if user is receiver and answered
        let username = UserManager.sharedInstance.getCredentials().username
        if let receiver = dialog.receiver {
            if receiver == username {
                name = addressManager.getNameFor(dialog.sender)
                self.questionLabel.text = "You sent \(name) this date:"
            }
        }
    }
    
}

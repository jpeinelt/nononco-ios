//
//  DialogOverviewController.swift
//  Nononco-iOS
//
//  Created by Julius on 14/02/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import UIKit

class DialogOverviewController: UIViewController {
    @IBOutlet weak var dialogTable: UITableView!
    @IBOutlet weak var addDialog: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        dialogTable.dataSource = self
        dialogTable.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("dailogsDidChange:"), name: "dialogsLoadedNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("addressBookUpdated:"), name: "addressBookReadSuccessfulNotification", object: nil)
        AddressBookManager.sharedInstance.readAddressBook()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func dailogsDidChange(notification: NSNotification) {
        self.dialogTable.reloadData()
    }

    @objc func addressBookUpdated(notification: NSNotification) {
        self.dialogTable.reloadData()
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

}



extension DialogOverviewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dialogs = DialogManager.sharedInstance.dialogs
        return dialogs.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let dialog = DialogManager.sharedInstance.dialogs[indexPath.row]
        if dialog.answer != nil {
            switch dialog.question {
            case .Date:
                var cell = tableView.dequeueReusableCellWithIdentifier("DialogDateCell") as? DialogDateCell
                if cell == nil {
                    let nib = NSBundle.mainBundle().loadNibNamed("DialogDateCell", owner: self, options: nil)
                    cell = (nib[0] as! DialogDateCell)
                }
                cell!.configureWith(dialog)
                return cell!
            case .Location:
                var cell = tableView.dequeueReusableCellWithIdentifier("DialogLocationCell") as? DialogLocationCell
                if cell == nil {
                    let nib = NSBundle.mainBundle().loadNibNamed("DialogLocationCell", owner: self, options: nil)
                    cell = (nib[0] as! DialogLocationCell)
                }
                cell!.configureWith(dialog)
                return cell!
            default:
                var cell = tableView.dequeueReusableCellWithIdentifier("DialogDateCell") as? DialogDateCell
                if cell == nil {
                    let nib = NSBundle.mainBundle().loadNibNamed("DialogDateCell", owner: self, options: nil)
                    cell = (nib[0] as! DialogDateCell)
                }
                cell!.configureWith(dialog)
                return cell!
            }
        } else {
            var cell = tableView.dequeueReusableCellWithIdentifier("DialogAnswerCell") as? DialogAnswerCell
            if cell == nil {
                let nib = NSBundle.mainBundle().loadNibNamed("DialogAnswerCell", owner: self, options: nil)
                cell = (nib[0] as! DialogAnswerCell)
            }
            cell!.configureWith(dialog)
            cell!.delegate = self
            return cell!
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let dialog = DialogManager.sharedInstance.dialogs[indexPath.row]
        if dialog.answer != nil {
            switch dialog.question {
            case .Date:
                return heightForDateCellWith(dialog)
            case .Location:
                return heightForLocationCellWith(dialog)
            default:
                return heightForDateCellWith(dialog)
            }
        } else {
            return heightForAnswerCellWith(dialog)
        }
    }

    func heightForDateCellWith(dialog: Dialog) -> CGFloat {
        var cell = self.dialogTable.dequeueReusableCellWithIdentifier("DialogDateCell") as? DialogDateCell
        if cell == nil {
            let nib = NSBundle.mainBundle().loadNibNamed("DialogDateCell", owner: self, options: nil)
            cell = (nib[0] as! DialogDateCell)
        }
        cell!.configureWith(dialog)
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        let size = cell?.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        return (size?.height)! + 1
    }

    func heightForLocationCellWith(dialog: Dialog) -> CGFloat {
        var cell = self.dialogTable.dequeueReusableCellWithIdentifier("DialogLocationCell") as? DialogLocationCell
        if cell == nil {
            let nib = NSBundle.mainBundle().loadNibNamed("DialogLocationCell", owner: self, options: nil)
            cell = (nib[0] as! DialogLocationCell)
        }
        cell!.configureWith(dialog)
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        let size = cell?.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        return (size?.height)! + 1
    }

    func heightForAnswerCellWith(dialog: Dialog) -> CGFloat {
        var cell = self.dialogTable.dequeueReusableCellWithIdentifier("DialogAnswerCell") as? DialogAnswerCell
        if cell == nil {
            let nib = NSBundle.mainBundle().loadNibNamed("DialogAnswerCell", owner: self, options: nil)
            cell = (nib[0] as! DialogAnswerCell)
        }
        cell!.configureWith(dialog)
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        let size = cell?.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        return (size?.height)! + 1
    }

}


extension DialogOverviewController: UITableViewDelegate {
}


extension DialogOverviewController: AnswerCellDelegate {
    func answerWasPressed(dialog: Dialog) {
        switch(dialog.question) {
        case .Date:
            let answerViewController = DateAnswerViewController()
            answerViewController.dialog = dialog
            self.showViewController(answerViewController, sender: self)
        case .Location:
            let answerViewController = LocationAnswerViewController()
            answerViewController.dialog = dialog
            self.showViewController(answerViewController, sender: self)
        default:
            let answerViewController = DateAnswerViewController()
            answerViewController.dialog = dialog
            self.showViewController(answerViewController, sender: self)
        }
    }
}


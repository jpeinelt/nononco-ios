//
//  CreateDialogViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 28/06/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import UIKit

class CreateDialogViewController: UIViewController {

    @IBOutlet weak var createDialogsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        createDialogsTable.dataSource = self
        createDialogsTable.delegate = self
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "CreateToAddressSegue" {
            // do something?
        }
    }
}

extension CreateDialogViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CreateDialogCell") as! CreateDialogCell
        let row = indexPath.row
        if let question = QuestionType(rawValue: row) {
            cell.label.text = Dialog.getQuestionFor(question)
        }
        return cell
    }
}

extension CreateDialogViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        DialogManager.sharedInstance.createNewDialogWith(indexPath.row)
        self.performSegueWithIdentifier("CreateToAddressSegue", sender: cell)
    }
}

//
//  Dialog.swift
//  Nononco-iOS
//
//  Created by Julius on 15/06/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation

enum QuestionType: Int {
    case Date
    case Location
    case Call
    case Image
    case Gif
    case Clip
    case String
    case Money
}

class Dialog {
    var _id: String?
    var sender: String
    var receiver: String?
    var question: QuestionType
    var answer: AnyObject?
    var createdAt: NSDate?
    var delivered: Int


    init(question: Int) {
        self.sender = UserManager.sharedInstance.getCredentials().username
        self.question = QuestionType(rawValue: question)!
        self.delivered = 0
    }

    init(id: String, sender: String, receiver: String, question: Int, answer: AnyObject?, createdAt: NSDate, delivered: Int) {
        self._id = id
        self.sender = sender
        self.receiver = receiver
        self.question = QuestionType(rawValue: question)!
        self.answer = answer
        self.createdAt = createdAt
        self.delivered = delivered
    }

    class func getQuestionFor(question: QuestionType) -> String {
        switch question {
        case .Date:
            return "At what time?"
        case .Location:
            return "Where is it?"
        case .Call:
            return "Please call me."
        default:
            return "At what time?"
        }
    }

    func JSONrepresentation() -> [String : AnyObject]? {
        guard self.receiver != nil else {
            return nil
        }
        var dict: [String: AnyObject] = ["sender": self.sender, "receiver": self.receiver!, "question": self.question.rawValue]
        if let id = self._id {
            dict["id"] = id
        }
        return dict
    }

    func answerAsJSON() -> [String: AnyObject]? {
        var dict:[String: AnyObject] = Dictionary()
        guard self.answer != nil else {
            return dict
        }
        switch self.question {
        case .Date:
            let date = self.answer as! NSDate
            dict["answer"] = Int(date.timeIntervalSince1970)
        case .Location:
            dict["answer"] = self.answer
        default:
            dict["answer"] = self.answer
        }
        return dict
    }
}
//
//  AddressbookViewController.swift
//  Nononco-iOS
//
//  Created by Julius on 09/07/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import UIKit
import Contacts

class AddressbookViewController: UIViewController {

    @IBOutlet weak var addressTable: UITableView!
    override func viewDidLoad() {
        addressTable.dataSource = self
        addressTable.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "addressBookUpdated:", name: "addressBookReadSuccessfulNotification", object: nil)
        AddressBookManager.sharedInstance.readAddressBook()
    }

    @objc func addressBookUpdated(notification: NSNotification) {
        self.addressTable.reloadData()
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}


extension AddressbookViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let addresses = AddressBookManager.sharedInstance.knownContacts
        let email = addresses[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("AddressBookCell") as! AddressBookCell
        cell.label.text = AddressBookManager.sharedInstance.getNameFor(email)
        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddressBookManager.sharedInstance.knownContacts.count
    }
}


extension AddressbookViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let addresses = AddressBookManager.sharedInstance.knownContacts
        let receiver = addresses[indexPath.row]
        DialogManager.sharedInstance.draft?.receiver = receiver
        DialogManager.sharedInstance.sendDraft()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}

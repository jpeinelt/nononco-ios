//
//  AddressBookManager.swift
//  Nononco-iOS
//
//  Created by Julius on 27/09/15.
//  Copyright © 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import Alamofire
import Contacts
import SwiftyJSON

class AddressBookManager {

    private let addressURL = Defines.baseURL + "addressBooks"

    var addressBook:[CNContact]
    var knownContacts: [String]

    static let sharedInstance = AddressBookManager()

    init() {
        addressBook = []
        knownContacts = []
        self.readAddressBook()
    }

    func readAddressBook() {
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName), CNContactEmailAddressesKey]
        let store = CNContactStore()
        store.requestAccessForEntityType(.Contacts) { (success, error) -> Void in
            if success == true {
                let containerId = CNContactStore().defaultContainerIdentifier()
                let predicate: NSPredicate = CNContact.predicateForContactsInContainerWithIdentifier(containerId)
                do {
                    self.addressBook = try store.unifiedContactsMatchingPredicate(predicate, keysToFetch: keysToFetch)
                    self.postAddressBook()
                } catch {
                    debugPrint("reading contact list failed")
                }
            }
        }
    }

    func postAddressBook() {
        let emailAdresses = self.getEmailAddressesForContacts()
        let creds = UserManager.sharedInstance.getCredentials()
        Alamofire.request(.POST, addressURL, parameters: ["contacts": emailAdresses], encoding: .JSON)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON { request, response, result in
                if result.isFailure {
                    debugPrint(result)
                    self.getContactsFromServer()    //TODO: why does it fail here????
                } else {
                    self.getContactsFromServer()
                }
        }

    }

    func getContactsFromServer() {
        let creds = UserManager.sharedInstance.getCredentials()
        Alamofire.request(.GET, addressURL)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON{request, response, result in
                if result.isFailure {
                    debugPrint(result)
                } else {
                    self.knownContacts = []
                    let json = JSON(result.value!)
                    for (_,subJson):(String, JSON) in json {
                        let email = subJson.stringValue
                        self.knownContacts.append(email)
                    }
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "addressBookReadSuccessfulNotification", object: nil))
                    })
                }
        }
    }

    func getEmailAddressesForContacts() -> [String] {
        var emails: [String] = []
        for contact in self.addressBook {
            if CNContactFormatter.stringFromContact(contact, style: .FullName) != nil {
                for emailEntry in contact.emailAddresses {
                    if let email = emailEntry.value as? String {
                        emails.append(email)
                    }
                }
            }
        }
        return emails
    }

    func getNameFor(email: String?) -> String {
        guard email != nil else {
            return ""
        }
        for contact in self.addressBook {
            for emailEntry in contact.emailAddresses {
                if let address = emailEntry.value as? String {
                    if address == email {
                        if let fullName = CNContactFormatter.stringFromContact(contact, style: .FullName) {
                            return fullName
                        }
                        return ""
                    }
                }
            }
        }
        return ""
    }
}

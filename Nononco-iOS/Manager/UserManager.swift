//
//  UserManager.swift
//  Nononco-iOS
//
//  Created by Julius on 15/06/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift

class UserManager {

    private let verifyURL: String = Defines.baseURL + "users/validate/"
    private let requestRegisterURL: String = Defines.baseURL + "users/register"
    private let deviceURL: String = Defines.baseURL + "users/device"

    private var _username: String?
    private let _password: String?
    var isRegistered: Bool {
        get {
            return NSUserDefaults().boolForKey("notFirstStart") && self._username != nil
        }
    }

    static let sharedInstance = UserManager()

    init() {
        let keychain = KeychainSwift()
        if let username = keychain.get("username") {
            _username = username
        } else {
            _username = nil
        }
        if let pass = keychain.get("password") {
            _password = pass
        } else {
            _password = NSUUID().UUIDString
        }
    }


    func requestRegistrationWithUsername(username: String, completionHandler: (Bool, ErrorType?) -> Void) {
        Alamofire.request(.POST, requestRegisterURL, parameters: ["username": username, "password": _password!], encoding: .JSON)
            .validate()
            .response { (request, response, data, error) in
                if (error != nil) {
                    debugPrint(error)
                    completionHandler(false, error)
                } else {
                    self._username = username
                    completionHandler(true, nil)
                }
        }
    }

    func registerWithPin(pin: String, email: String, completionHandler: (Bool, ErrorType?) -> Void) {
        self._username = email
        let url = verifyURL + pin
        Alamofire.request(.POST, url, parameters: ["username": _username!, "password": _password!], encoding: .JSON)
            .validate()
            .response { (request, response, data, error) in
                if (error != nil) {
                    debugPrint(error)
                    completionHandler(false, error)
                } else {
                    self.saveCredentials()
                    NSUserDefaults().setBool(true, forKey: "notFirstStart")
                    completionHandler(true, nil)
                }
        }
    }

    func getCredentials() -> (username: String, password: String) {
        return (_username!, _password!)
    }


    func saveCredentials() {
        let keychain = KeychainSwift()
        keychain.set(self._username!, forKey: "username", withAccess: .AccessibleAfterFirstUnlock)
        keychain.set(self._password!, forKey: "password", withAccess: .AccessibleAfterFirstUnlock)
    }

    func registerDeviceAtBackend(deviceToken: NSData) {
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        for var i = 0; i < deviceToken.length; i++ {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        Alamofire.request(.POST, deviceURL, parameters: ["token": tokenString, "username": _username!, "password": _password!], encoding: .JSON)
            .validate()
            .response { (request, response, data, error) in
                debugPrint(response)
        }
    }

}

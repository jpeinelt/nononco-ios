//
//  DialogManager.swift
//  Nononco-iOS
//
//  Created by Julius on 08/06/15.
//  Copyright (c) 2015 Mr Fridge Software. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DialogManager {

    private let dialogsURL = Defines.baseURL + "dialogs/"

    var dialogs: Array<Dialog>
    var draft: Dialog?

    static let sharedInstance = DialogManager()

    init() {
        dialogs = []
        fetchNewDialogs()
    }

    func fetchNewDialogs() {
        var tempDialogs = [Dialog]()
        let creds = UserManager.sharedInstance.getCredentials()
        let url = dialogsURL + "pending"
        Alamofire.request(.GET, url)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON { request, response, result in
                if result.isFailure {
                    debugPrint(result)
                    //TODO: show warning
                } else {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
                    let json = JSON(result.value!)
                    for (_,subJson):(String, JSON) in json {
                        let id = subJson["_id"].stringValue
                        let sender = subJson["sender"].stringValue
                        let receiver = subJson["receiver"].stringValue
                        let question = subJson["question"].intValue
                        var answer: AnyObject?
                        if (subJson["answer"].null != nil) {
                            answer = nil
                        } else {
                            answer = subJson["answer"].object
                        }
                        var date = dateFormatter.dateFromString(subJson["createdAt"].stringValue)
                        if date == nil {
                            date = NSDate()
                        }
                        let delivered = subJson["delivered"].intValue
                        let newDialog = Dialog(id: id, sender: sender, receiver: receiver, question: question, answer: answer, createdAt: date!, delivered: delivered)
                        tempDialogs.append(newDialog)
                    }
                    self.dialogs.removeAll()
                    self.dialogs.appendContentsOf(tempDialogs.sort({$0.createdAt!.compare($1.createdAt!) == NSComparisonResult.OrderedDescending}))
                    self.dialogsLoaded()
                }
            }
    }

    func sendDialog(dialog: Dialog) {
        let creds = UserManager.sharedInstance.getCredentials()
        Alamofire.request(.POST, dialogsURL, parameters: dialog.JSONrepresentation(), encoding: .JSON)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON { request, response, result in
                if result.isFailure {
                    debugPrint(result)
                }
                self.fetchNewDialogs()
        }
    }

    func answerDialog(dialog: Dialog) {
        guard dialog._id != nil && dialog.answer != nil else {
            return
        }
        let creds = UserManager.sharedInstance.getCredentials()
        let url = dialogsURL + "answer/" + dialog._id!
        if let parameters = dialog.answerAsJSON() {
            Alamofire.request(.PUT, url, parameters: parameters, encoding: .JSON)
                .authenticate(user: creds.username, password: creds.password)
                .validate()
                .responseJSON { request, response, result in
                    if result.isFailure {
                        debugPrint(result)
                    }
                    self.fetchNewDialogs()
            }
        }
    }

    func ingnoreDialog(dialog: Dialog) {
        guard dialog._id != nil else {
            return
        }
        let ignoreURL = dialogsURL + "/ignore/" + dialog._id!
        let creds = UserManager.sharedInstance.getCredentials()
        Alamofire.request(.POST, ignoreURL)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON { request, response, result in
                if result.isFailure {
                    debugPrint(result)
                }
                self.fetchNewDialogs()

        }
    }


    func dialogsLoaded() {
        dispatch_async(dispatch_get_main_queue(), {
            NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "dialogsLoadedNotification", object: nil))
        })
    }

    func createNewDialogWith(question: Int) {
        draft = Dialog(question: question)
    }

    func sendDraft() {
        guard self.draft != nil else {
            return
        }
        let creds = UserManager.sharedInstance.getCredentials()
        Alamofire.request(.POST, dialogsURL, parameters: self.draft!.JSONrepresentation(), encoding: .JSON)
            .authenticate(user: creds.username, password: creds.password)
            .validate()
            .responseJSON { request, response, result in
                if result.isFailure {
                    debugPrint(result)
                }
                self.draft = nil
                self.fetchNewDialogs()
        }
    }

    func loadMockDialogData() {
        if let path = NSBundle.mainBundle().pathForResource("mockDialogs", ofType: "json"),
            let jsonData = NSData(contentsOfFile: path) {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
                let json = JSON(data: jsonData)
                for (_,subJson):(String, JSON) in json {
                    let id = subJson["id"].stringValue
                    let sender = subJson["sender"].stringValue
                    let receiver = subJson["receiver"].stringValue
                    let question = subJson["question"].intValue
                    var answer: AnyObject?
                    if (subJson["answer"].null != nil) {
                        answer = nil
                    } else {
                        answer = subJson["answer"].object
                    }
                    var date = dateFormatter.dateFromString(subJson["createdAt"].stringValue)
                    if date == nil {
                        date = NSDate()
                    }
                    let delivered = subJson["delivered"].intValue
                    let newDialog = Dialog(id: id, sender: sender, receiver: receiver, question: question, answer: answer, createdAt: date!, delivered: delivered)
                    self.dialogs.append(newDialog)
                }
        }
    }
}

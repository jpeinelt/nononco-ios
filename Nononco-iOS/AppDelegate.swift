//
//  AppDelegate.swift
//  Nononco-Client
//
//  Created by Julius Peinelt on 05/02/15.
//  Copyright (c) 2015 Mr Fridge. All rights reserved.
//

import UIKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var deviceRegistered: Bool = false

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var viewController: UIViewController
        if UserManager.sharedInstance.isRegistered {
            viewController = storyboard.instantiateViewControllerWithIdentifier("RegisteredPath")
        } else {
            viewController = storyboard.instantiateViewControllerWithIdentifier("UnregisteredPath")
        }

        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()

        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()

        return true
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        if UserManager.sharedInstance.isRegistered && self.deviceRegistered == false {
            UserManager.sharedInstance.registerDeviceAtBackend(deviceToken)
            self.deviceRegistered = true
        }
    }


    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        debugPrint(error.description)
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        DialogManager.sharedInstance.fetchNewDialogs()
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }

    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        if url.host == "verify" {
            if let pathComponents = url.path?.componentsSeparatedByString("/") {
                guard pathComponents.count == 3  else {
                    return false
                }
                UserManager.sharedInstance.registerWithPin(pathComponents[2], email: pathComponents[1], completionHandler: {
                    (success, error) -> Void in
                    if (success) {
                        UserManager.sharedInstance.saveCredentials()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewControllerWithIdentifier("RegisteredPath")
                        self.window?.rootViewController = viewController
                    } else {
                        let alert = UIAlertController(title: "Verification Error", message: error.debugDescription, preferredStyle: UIAlertControllerStyle.Alert)
                        let action = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                            alert.dismissViewControllerAnimated(true, completion: nil)
                        })
                        alert.addAction(action)
                        self.window?.rootViewController!.presentViewController(alert, animated: true, completion: nil)
                    }
                })
                return true
            }
        }
        return false
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS dialog) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        if UserManager.sharedInstance.isRegistered {
            DialogManager.sharedInstance.fetchNewDialogs()
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}


